import numpy as np
import cudf
import pandas as pd
from cuml.tsa.arima import ARIMA
import itertools
import csv



df = cudf.read_csv("timeTest.csv").astype(np.float64)

reducedSet = []

reducedSet = df.iloc[::3]

q = range(0, 3)
d = range(0, 3)
p = range(0, 3)

Q = range(0, 3)
D = range(0, 3)
P = range(0, 1)

orbit = [189]
pdq = list(itertools.product(p, d, q))
seasonal_pdq = list(itertools.product(P, D, Q, orbit))

#seasonal_pdq = [(x[0], x[1], x[2], 189) for x in list(itertools.product(p, d, q))]

BIC = []
AIC = []
SARIMAX_model = []
counter = 0
for param in pdq:
    for param_seasonal in seasonal_pdq:
        if param[1]+param_seasonal[1] <= 2:
            model = ARIMA(reducedSet, order = param, seasonal_order = param_seasonal)
            model.fit(opt_disp = 1, maxiter = 25) 
            AIC.append(str(model.aic))
            BIC.append(str(model.bic))
            SARIMAX_model.append([param, param_seasonal])

        print("----------------------------------------------------------")
        print("Just finished fitting nr: ", counter, " out of: ", len(pdq)*len(seasonal_pdq))
        print("----------------------------------------------------------")
        counter = counter + 1



p = []
d = []
q = []

P = []
D = []
Q = []


for i,v in enumerate(SARIMAX_model):
    p.append(SARIMAX_model[i][0][0])
    d.append(SARIMAX_model[i][0][1])
    q.append(SARIMAX_model[i][0][2])
    P.append(SARIMAX_model[i][1][0])
    D.append(SARIMAX_model[i][1][1])
    Q.append(SARIMAX_model[i][1][2])

    
data = pd.DataFrame()
data["p"] = p
data["d"] = d
data["q"] = q
data["P"] = P
data["D"] = D
data["Q"] = Q
data["AIC"] = AIC
data["BIC"] = BIC

data.to_csv("simResults.csv",index = False)



    
 
