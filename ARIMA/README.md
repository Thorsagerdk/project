Requires anaconda and cuda drivers

To activate rapids environment type:
conda activate rapids-0.19

rapids environment:
https://rapids.ai/start.html#get-rapids 


further requirements:

conda install pandas

conda install more-itertools
