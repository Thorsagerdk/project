"""
CMC (Charging Multiplier Calculator) and battery model
"""
#-Libraries---------------------------------------------------------------------

import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt

#-MatPlotLib configuration------------------------------------------------------

plt.rcParams['figure.figsize'] = (20.0, 10.0)
plt.rcParams.update({'font.size': 12})
plt.style.use('ggplot')

#-Global variables--------------------------------------------------------------

simTS = 10 #[Hz] sample frequency of data
periodLength = 5670 # rounded down to nearest multiple of 10
simLength = 5*periodLength #[s] length of sunlight period / simulation
sunPeriod = 3670
powerIn = [] #[W] array for input power/wattage
powerOut = [] #[W] power output array
debugIL = []
LSTMPrediction = []

#-Import data-------------------------------------------------------------------

# Power production and consumption from CSV file to arrays
data = pd.read_csv("cmc_input_data.csv", engine="python")
for i in range(0,int(simLength/simTS)):
    powerOut.append(data['output_W'][i])
    powerIn.append(data['generation_W'][i])
    LSTMPrediction.append(data['generation_W'][i])

# load file storing SOC-OCV lookup table generated for 25degC
ocv_lookup = pd.read_csv("soc_ocv_25degc.csv", skiprows = 1,
                names =['SOC','OCV','OCV4'],index_col=False,engine='python')

#-BatteryPack class definition--------------------------------------------------

class BatteryPack:
    cell_Rs = 0.075 #[Ohm] Resistance of a cell measured at 25degC after 20s pulse
    cellCapacity = 2.6 #[Ah] Considered capacity of a cell at beggining of life
    OCV = 0 # OCV for 1 cell
    SOC = 0

    # Constructor function
    def __init__(self, serial, parallel, initSOC):

        self.serialCells = serial
        self.parallelCells = parallel
        self.Rs = (self.cell_Rs*self.serialCells)/self.parallelCells
        self.capacity = parallel*self.cellCapacity #[Ah] capacity of battery pack
        self.prev_SOC = initSOC #[0-1] set initial state of charge
        self.OCV = np.interp(initSOC, ocv_lookup.SOC, ocv_lookup.OCV4)
        self.voltage = self.OCV

    # Ideal load calculator
    def calculateIdealLoad(self, goalSOC, initialSOC):
        idealILs = []
        for i in range (int(sunPeriod/simTS)):
            f = (3600*(self.prev_SOC-goalSOC)*self.capacity)/(3670-(i*10))
            idealILs.append(f)

            self.voltage = self.OCV - self.Rs * idealILs[-1]
            self.SOC = self.prev_SOC - (idealILs[-1] * simTS)/(self.capacity * 3600)
            self.prev_SOC = self.SOC

        #calculate ideal current load average
        idealIL = np.average(idealILs)

        #init anew
        self.prev_SOC = initialSOC #[0-1] set initial state of charge
        self.OCV = np.interp(initialSOC, ocv_lookup.SOC, ocv_lookup.OCV4)
        self.voltage = self.OCV

        return idealIL

    def calculateCM(self, timeCounter, predictedMaxLoad, idealLoad):

        CMC = idealLoad/((predictedMaxLoad/self.voltage)/self.parallelCells)
        if(CMC > 1):
            CMC = 1
        if(CMC < 0):
            CMC = 1

        print("Max load: %f" % predictedMaxLoad)
        print("Ideal load: %f" % idealLoad)
        print("CMC: %f" % CMC)

        return CMC

    def charge(self, input, timeStep, t):
        IL = (input/self.voltage)/self.parallelCells
        self.OCV = np.interp(self.prev_SOC, ocv_lookup.SOC, ocv_lookup.OCV4)
        self.voltage = self.OCV - self.Rs * IL
        self.SOC = self.prev_SOC - (IL * timeStep)/(self.capacity * 3600)
        self.prev_SOC = self.SOC
        debugIL.append(IL)

#-Main--------------------------------------------------------------------------

outdata_time = [] # Output array for timekeeping
outdata_soc = [] # State of charge output array
cmc_out = []

battery = BatteryPack(4,2,0.75)

for globalTime in range(0, simLength, periodLength):

    currentLoad = battery.calculateIdealLoad(0.85, battery.prev_SOC)
    print("\nIdeal load: %f" % currentLoad)
    for i in range(int(periodLength/simTS)):

        predictedInput = powerOut[i + int(globalTime/simTS)] - LSTMPrediction[i + int(globalTime/simTS)]

        CMC = battery.calculateCM((i + int(globalTime/simTS)), predictedInput, currentLoad)
        cmc_out.append(CMC)

        realInput = (powerOut[i + int(globalTime/simTS)] - powerIn[i + int(globalTime/simTS)])*CMC
        print("actual load: %f" % realInput)
        if(i < sunPeriod/10):
            battery.charge(realInput, simTS, i + int(globalTime/simTS))
            outdata_time.append(i * simTS + globalTime)
            outdata_soc.append(battery.SOC)
        else:
            realInput = powerOut[i + int(globalTime/simTS)] - powerIn[i + int(globalTime/simTS)]
            battery.charge(realInput, simTS, i + int(globalTime/simTS))
            outdata_time.append(i * simTS + globalTime)
            outdata_soc.append(battery.SOC)

#-Plot output-------------------------------------------------------------------
plt.hlines(0.85, 0, len(outdata_time)*simTS, linestyles='dashed', label='SOC limit')
plt.plot(outdata_time, outdata_soc, label='SOC [%]')
plt.plot(outdata_time, debugIL, label='IL [A]')
plt.plot(outdata_time, cmc_out, 'y', label='CMC')
plt.xlabel('Time')
plt.legend()
plt.show()
