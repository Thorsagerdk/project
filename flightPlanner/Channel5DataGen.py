import csv
from array import *
import matplotlib.pyplot as plt
import numpy as np
import random
import time as clock

random.seed(clock.time())

dT = 3670
uT = 2005

baseLine = 1.4
lG = [2.4, 33.58]
mG = [3.7, 42]
sHG = [8.1, 12.52]
hG = [13.8, 11.9]

numbOfPeriods = 1000
resolution = 10
offset = 0




def getGroup(val):
    if(val < lG[1]):
        variance = random.uniform(-0.2,0.2)
        return lG[0] + variance
    elif(val >= lG[1] and val < lG[1] + mG[1]):
        variance = random.uniform(-0.2,0.2)
        return mG[0] + variance
    elif(val >= lG[1] + mG[1] and val < lG[1] + mG[1] + sHG[1]):
        variance = random.uniform(-0.3,0.3)
        return sHG[0] + variance
    elif(val >= lG[1] + mG[1] + sHG[1]):
        variance = random.uniform(-0.4,0.4)
        return hG[0] + variance
    else:
        return 1.4

def createNoise(val):
    noise = random.uniform(-0.1,0.2)
    return val+noise


data = []
x = 0
X = []
groupVal = getGroup(random.uniform(0,100))

for i in range(numbOfPeriods):
    #add base line values
    for base in range(int(dT/resolution)):
        data.append(createNoise(baseLine))
        X.append(x)
        x += resolution
    groupVal = getGroup(random.uniform(0,100))
    #add spike values
    for spike in range(int(uT/resolution)):
        data.append(createNoise(groupVal))
        X.append(x)
        x += resolution
with open('Channel5Consumption10FreqWithVariance.csv', 'w', newline='') as train_file:
    writer = csv.writer(train_file)
    writer.writerow(["Watt"])
    for i in range(len(data)):
        writer.writerow([data[i]])


plt.plot(X, data)
plt.show()
