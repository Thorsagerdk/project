import csv
from array import *
import matplotlib.pyplot as plt
import numpy as np
import random
import time as clock

random.seed(clock.time())


periodLength = 1165.25429424944

baseLine = 0.75

lG = [1.145, 12.41]
mG = [1.246, 35.18]
hG = [1.356, 52.41]

numbOfPeriods = 1000
resolution = 10


def getGroup(val):
    if(val < lG[1]):
        variance = random.uniform(-0.03,0.03)
        return lG[0] + variance
    elif(val >= lG[1] and val < (lG[1] + mG[1])):
        variance = random.uniform(-0.04,0.04)
        return mG[0] + variance
    elif(val >= (lG[1] + mG[1])):
        variance = random.uniform(-0.05,0.05)
        return hG[0] + variance
    else:
        print("error" , val)
        return baseLine

def createNoise(val):
    noise = random.uniform(-0.01,0.01)
    return val + noise


data = []
x = 0
X = []
groupVal = 0

for i in range(numbOfPeriods):
    #add base line value
    data.append(createNoise(baseLine))
    X.append(x)
    x += resolution
    #create rising values going to randomly selected group
    groupVal = getGroup(random.uniform(0,100))

    risingEntries = int((periodLength/3)/resolution)
    increaseStep = (groupVal - baseLine)/risingEntries
    nextVal = baseLine + increaseStep
    for rising in range(risingEntries):
        data.append(nextVal)
        nextVal += increaseStep
        X.append(x)
        x += resolution

    for flat in range(int((periodLength/3)/resolution)):
        data.append(createNoise(groupVal))
        X.append(x)
        x += resolution

    decreasingEntries = int((periodLength/3)/resolution)
    decreaseStep = increaseStep
    nextVal = groupVal - decreaseStep
    for decreasing in range(decreasingEntries):
        data.append(nextVal)
        nextVal -= decreaseStep
        X.append(x)
        x += resolution

with open('Channel6Consumption10FreqWithVarianceThirdFlat.csv', 'w', newline='') as dataFile:
    writer = csv.writer(dataFile)
    writer.writerow(["Watt"])
    for i in range(len(data)):
        writer.writerow([data[i]])

plt.plot(X, data, label="third")


# data = []
# x = 0
# X = []
# groupVal = 0
#
# for i in range(numbOfPeriods):
#     #add base line value
#     data.append(createNoise(baseLine))
#     X.append(x)
#     x += resolution
#     #create rising values going to randomly selected group
#     groupVal = getGroup(random.uniform(0,100))
#
#     risingEntries = int((periodLength/4)/resolution)
#     increaseStep = (groupVal - baseLine)/risingEntries
#     nextVal = baseLine + increaseStep
#     for rising in range(risingEntries):
#         data.append(nextVal)
#         nextVal += increaseStep
#         X.append(x)
#         x += resolution
#
#     for flat in range(int((periodLength/2)/resolution)):
#         data.append(createNoise(groupVal))
#         X.append(x)
#         x += resolution
#
#     decreasingEntries = int((periodLength/4)/resolution)
#     decreaseStep = increaseStep
#     nextVal = groupVal - decreaseStep
#     for decreasing in range(decreasingEntries):
#         data.append(nextVal)
#         nextVal -= decreaseStep
#         X.append(x)
#         x += resolution
#
# with open('Channel6Consumption10FreqWithVarianceHalfFlat.csv', 'w', newline='') as dataFile:
#     writer = csv.writer(dataFile)
#     writer.writerow(["Watt"])
#     for i in range(len(data)):
#         writer.writerow([data[i]])
#
# plt.plot(X, data, label="half")
#
#
# data = []
# x = 0
# X = []
# groupVal = 0
#
# for i in range(numbOfPeriods):
#     #add base line value
#     data.append(createNoise(baseLine))
#     X.append(x)
#     x += resolution
#     #create rising values going to randomly selected group
#     groupVal = getGroup(random.uniform(0,100))
#
#     risingEntries = int((periodLength/2)/resolution)
#     increaseStep = (groupVal - baseLine)/risingEntries
#     nextVal = baseLine + increaseStep
#     for rising in range(risingEntries):
#         data.append(nextVal)
#         nextVal += increaseStep
#         X.append(x)
#         x += resolution
#
#     # for flat in range(int((periodLength/2)/resolution)):
#     #     data.append(createNoise(groupVal))
#     #     X.append(x)
#     #     x += resolution
#
#     decreasingEntries = int((periodLength/2)/resolution)
#     decreaseStep = increaseStep
#     nextVal = groupVal - decreaseStep
#     for decreasing in range(decreasingEntries):
#         data.append(nextVal)
#         nextVal -= decreaseStep
#         X.append(x)
#         x += resolution
#
# with open('Channel6Consumption10FreqWithVariance.csv', 'w', newline='') as dataFile:
#     writer = csv.writer(dataFile)
#     writer.writerow(["Watt"])
#     for i in range(len(data)):
#         writer.writerow([data[i]])
#
# plt.plot(X, data, label="spike")




# spike = []
# third = []
# half = []
# XSpike = []
# XHalf = []
# XThird = []
# x = 0
# with open('Channel6Consumption10FreqWithVarianceThirdFlat.csv') as third_file:
#     third_reader = csv.reader(third_file, delimiter=',')
#     i = 0
#     for row in third_reader:
#         if(i == 0):
#             i+= 1
#         else:
#             third.append(row[0])
#             XThird.append(x)
#             x +=1
# with open('Channel6Consumption10FreqWithVariance.csv') as spike_file:
#     spike_reader = csv.reader(spike_file, delimiter=',')
#     i = 0
#     x= 0
#     for row in spike_reader:
#         if(i == 0):
#             i+= 1
#         else:
#             spike.append(row[0])
#             XSpike.append(x)
#             x += 1
# with open('Channel6Consumption10FreqWithVarianceHalfFlat.csv') as half_file:
#     half_reader = csv.reader(half_file, delimiter=',')
#     i = 0
#     x = 0
#     for row in half_reader:
#         if(i == 0):
#             i+= 1
#         else:
#             half.append(row[0])
#             XHalf.append(x)
#             x+= 1

#plt.plot(XSpike, spike)
#plt.plot(XThird, third)
#plt.plot(XSpike, half)
plt.show()
